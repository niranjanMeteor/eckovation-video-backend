var HttpApiReturnCodes						= require('./http_api_return_codes.js');
var errorCodes 										= require('./error_codes.js');


module.exports = {
	sendError : function(res,err,error_index,status_code) {
		if(typeof(status_code) == "undefined" ) {
			status_code = HttpApiReturnCodes.SERVER_ERROR;
		}

		res.set("KeepAlive", false);
		res.status(status_code).json({
			code 	: errorCodes[error_index][0],
			message : errorCodes[error_index][1],
			success : false
		});
		return;
	},
	sendSuccess : function(res,data) {
		res.set("KeepAlive", false);
		res.status(HttpApiReturnCodes.OK).json({
			success : true,
			data	: data
		});
		return;
	}
};