var HttpStatusCodes = {

  OK                      : 200,
  BAD_REQUEST             : 400,
  SERVER_ERROR            : 500,
  NOT_FOUND               : 404,
  FORBIDDEN               : 403,
  UNAUTHORIZED            : 401,
  DEVICE_TOKEN_EXISTS     : 492,
  TOKEN_EXPIRED           : 498,
  INVALID_PARAMETER       : 600,
  TOO_MANY_REQUESTS       : 429,
  SERVER_SPECIFIC_ERROR   : 700,
  NO_DATA                 : 900,
  NO_INPUT_DATA           : 1000,
  USER_SUBSCRIPTION_REQUIRED : 350,
  USER_SUBSCRIPTION_EXPIRED : 399
};


module.exports = HttpStatusCodes;