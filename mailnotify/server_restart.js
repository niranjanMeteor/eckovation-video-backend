var mail_client             = require('../mail/mail_client.js');
var configs                 = require('../mail/configs.js');
var ip                      = require('ip');
var os                      = require("os");

var html_string = '<h4>'+ Date()  +': VIDEO-PLUGIN Service has Restarted </h4>';
var myIpAddres  = ip.address(); 
var hostname    = os.hostname();

var mailOptions = {
    from    : configs.TECH_FROM,
    to      : configs.TECH_TO,
    subject : myIpAddres+' : '+hostname+' : VIDEO-PLUGIN Service Restarted',
    html    : html_string
};

mail_client.send(mailOptions, function(err, response){
    if (err) {
    	console.trace(err);
        console.log('Server Restart : Mail not sent successfully',err);
    } else {
        console.log('Server Restart : Mail sent successfully');
    }
});