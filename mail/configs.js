var fs                                = require('fs');

var configs = {

	CONFIGS_OVERWRITE_FILE              : "configs_overwrite.js",
	ANALYTICS_SMTP_HOST									: 'smtp.zoho.com',
	ANALYTICS_SMTP_PORT									: 465,
	ANALYTICS_SMTP_USERNAME							: 'hi@eckovation.com',
	ANALYTICS_SMTP_PASSWORD							: 'eckovationECKOVATION987@123',
	ANALYTICS_FROM											: 'Eckovation Analytics <hi@eckovation.com>',
	ANALYTICS_TO 												: 'akshatg@eckovation.com,riteshs@eckovation.com,niranjan@eckovation.com,ritula@eckovation.com',
	TECH_FROM				    								: 'Eckovation Tech <hi@eckovation.com>',
	TECH_TO															: 'akshatg@eckovation.com,niranjan@eckovation.com'

};

var overwriteConfigFulFileName  = __dirname+'/'+configs.CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		configs[key] = overwriteConfig[key];
	}
} else {
	console.log('No Overwrite Configs File Found to overwrite any Application configs key');
}

module.exports = configs;