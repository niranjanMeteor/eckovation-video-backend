var nodemailer 				= require('nodemailer');
var smtpTransport 		= require('nodemailer-smtp-transport');
var configs					  = require('./configs.js');

var transporter = nodemailer.createTransport(smtpTransport({
    host		: configs.ANALYTICS_SMTP_HOST,
    port		: configs.ANALYTICS_SMTP_PORT,
    secure 		: true,
    auth		: {
        user: configs.ANALYTICS_SMTP_USERNAME,
        pass: configs.ANALYTICS_SMTP_PASSWORD
    }
}));

exports.send = function (mailOptions, cb) {
  transporter.sendMail(mailOptions, function(error, info){
    if(error){
        console.trace(error);
      cb(error, null);
    }
    else {
    	cb(null, info);
    }
  });
}

exports.inform_db_crash = function(subject, body, cb){
  var mailOptions = {
    from    : configs.SERVER_FROM,
    to      : configs.TECH_TO,
    subject : subject, 
    html    : body
  };
  transporter.sendMail(mailOptions, function(error, info){
    if(error){
      return cb(error, null);
    }
    else {
      return cb(null, info);
    }
  });
}

exports.informTechTeam = function(subject, body){
  var bodyHtml;
  if(process.env.NODE_ENV == "ci" || process.env.NODE_ENV == "dapi") {
    return;
  }
  if(typeof(body) == 'object'){
    bodyHtml = JSON.stringify(body);
  } else {
    bodyHtml = body;
  }
  var mailOptions = {
    from    : configs.SERVER_FROM,
    to      : configs.TECH_TO,
    subject : subject, 
    html    : bodyHtml
  };
  transporter.sendMail(mailOptions, function(error, info){
    if(error){
      console.trace(error);
    }
    console.log('Successfully Informed the tech about subject');
    console.log(subject);
    return;
  });
}