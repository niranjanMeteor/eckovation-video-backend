var fs                                = require('fs');

var configs = {

	CONFIGS_OVERWRITE_FILE                    : "configs_overwrite.js",
	PLUGIN_ID                          				: "57a1d59b73a3e04128bb58ee",
	SERVER_PORT                               : 3009,
	JWT_ACCESS_TOKEN_PRIVATE_KEY       				: "eckovationECKOVATION654321123456ECKOVATIONeckovation",
	JWT_ACCESS_TOKEN_EXPIRY_TIME       				: 60*30,     // 30 MINUTES
	JWT_ACCESS_TOKEN_HCODE_LENGTH      				: 20,
	MORGAN_LOG_DIRECTORY               				: "/var/log/videoplugin/daily",
	VIDEOPLUGIN_SERVER_SERVICE_NAME 		      : 'videoplugin-server',
	VIDEOPLUGIN_SERVER_LOG_DIR 								: '/var/log/videoplugin',
	VIDEOPLUGIN_SERVER_SERVICE_USER    				: 'ubuntu',
	VIDEOPLUGIN_SERVER_HEAP_SIZE_MAX   				: 890,
	ECKOVATION_API_KEY                        : "UGHSDO7RENVERVN89dfjr890ng4guw85uw40nw58w485980",
	PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY       	: "eckovationECKOVATION654321123456ECKOVATIONeckovation",

};

var overwriteConfigFulFileName  = __dirname+'/'+configs.CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		configs[key] = overwriteConfig[key];
	}
} else {
	console.log('No Overwrite Configs File Found to overwrite any Application configs key');
}

module.exports = configs;