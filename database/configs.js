var fs                                = require('fs');

var DbConfigs = {

	DB_CONFIGS_OVERWRITE_FILE           : "configs_overwrite.js",
	DB_NAME 														: "videoplugin",
	MONGO_HOST                          : "127.0.0.1",
	MONGO_PORT                          : "27017",
	MONGO_UNAME													: "",
	MONGO_PASS													: "",
	MONGO_POOLSIZE                      : 10
};

var overwriteConfigFulFileName  = __dirname+'/'+DbConfigs.DB_CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		DbConfigs[key] = overwriteConfig[key];
	}
} else {
	console.log('No Overwrite Configs File Found to overwrite any Database configs key');
}

module.exports = DbConfigs;