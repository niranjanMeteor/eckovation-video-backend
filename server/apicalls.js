var https             			      		= require('https');
var http             			        		= require('http');

var PluginConfigs					      			= require('../configs/configs.js');
var ServerConfigs                			= require('./configs.js');
var ApiPaths                      		= require('./apipaths.js');
var Credentials         							= require('./credentials.js');

exports.verifyIdentifier = function(identifier, cb){
	var plugin_id  = PluginConfigs.PLUGIN_ID;
	var params  = {
		client_id     : Credentials.CLIENT_ID,
		client_sec    : Credentials.CLIENT_SECRET,
		pl_id         : plugin_id,
		identifier    : identifier              
	};

	var options = {
	  host       : ServerConfigs.SERVER_HOST,
	  port       : ServerConfigs.SERVER_PORT,
	  path       : ApiPaths.USER_IDENTIFIER_VERIFY,
	  method     : 'POST'
	};

	var data = JSON.stringify(params);
	options.headers = {
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(data)
  };

	var req = http.request(options);
	req.write(data);

	var responseData = '';
	req.on('response', function(res){
	  res.on('data', function(chunk){
	   responseData += chunk;
	  });
	  res.on('end', function(){
	    var decodedResponse = JSON.parse(responseData);
	    console.log(decodedResponse);
	    if(decodedResponse.success == true){
	    	console.log('Successfully Verified Identifier for identifier : '+identifier);
	    	return cb(null,{success:true,data:decodedResponse.data});
	    } else {
	    	console.log('Could Not Verify Identifier for identifier : '+identifier);
	    	console.log(decodedResponse);
	    	return cb(null,{success:false});
	    }
	  });
	});
	req.end();
	req.on('error', function(err){
		console.log('Error Occurred in Verifying Identifier for identifier : '+identifier);
    console.log(err);
    return cb(null,{success:false});
  });
};

exports.checkIfAdmin = function(identifier, cb){
	var plugin_id  = PluginConfigs.PLUGIN_ID;
	var params  = {
		client_id     : Credentials.CLIENT_ID,
		client_sec    : Credentials.CLIENT_SECRET,
		pl_id         : plugin_id,
		identifier    : identifier              
	};

	var options = {
	  host       : ServerConfigs.SERVER_HOST,
	  port       : ServerConfigs.SERVER_PORT,
	  path       : ApiPaths.USER_IDENTIFIER_VERIFY_IF_GROUPADMIN,
	  method     : 'POST'
	};

	var data = JSON.stringify(params);
	options.headers = {
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(data)
  };

	var req = http.request(options);
	req.write(data);

	var responseData = '';
	req.on('response', function(res){
	  res.on('data', function(chunk){
	   responseData += chunk;
	  });
	  res.on('end', function(){
	    var decodedResponse = JSON.parse(responseData);
	    console.log(decodedResponse);
	    if(decodedResponse.success == true){
	    	console.log('Successfully Checked IfAdmin for identifier : '+identifier);
	    	return cb(null,{success:true,is_admin:decodedResponse.data.is_admin});
	    } else {
	    	console.log('Could Not Check IfAdmin for identifier : '+identifier);
	    	console.log(decodedResponse);
	    	return cb(null,{success:false});
	    }
	  });
	});
	req.end();
	req.on('error', function(err){
		console.log('Error Occurred in Checking IfAdmin for identifier : '+identifier);
    console.log(err);
    return cb(null,{success:false});
  });
};

