var ApiPaths  = {
	USER_IDENTIFIER_VERIFY           								: "/plugin/verify_usr_idnt",
	USER_IDENTIFIER_VERIFY_IF_GROUPADMIN           	: "/plugin/check_adm"
};

module.exports = ApiPaths;