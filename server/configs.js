var fs                        = require('fs');

var ServerConfigs = {

	SRV_CONFIGS_OVERWRITE_FILE  : "configs_overwrite.js",
	SERVER_HOST                 : "192.168.1.25",
	SERVER_PORT                 : 3000

};

var overwriteConfigFulFileName  = __dirname+'/'+ServerConfigs.SRV_CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		ServerConfigs[key] = overwriteConfig[key];
	}
} else {
	console.log('No Overwrite Configs File Found to overwrite any Server configs key');
}

module.exports = ServerConfigs;