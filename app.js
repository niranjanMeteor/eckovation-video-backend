/*  ************************* APP BASIC PACKAGES *******************************    */
var express                     = require('express');
var mongoose                    = require('mongoose');
var expressValidator            = require('express-validator');
var path                        = require('path');
var favicon                     = require('serve-favicon');
var cookieParser                = require('cookie-parser');
var bodyParser                  = require('body-parser');
var fs                          = require('fs');
var morgan                      = require('morgan');
var cors                        = require('cors');
var compression                 = require('compression');
var os                          = require('os');
/*  ****************************************************************************    */





/*  ***************************** APP ROUTES  **********************************    */
var chapter                     = require('./routes/chapter');
var topic                       = require('./routes/topic');
var user                        = require('./routes/user');
var userchapter                 = require('./routes/userchapter');
var userchaptertopic            = require('./routes/userchaptertopic');
var eckovation                  = require('./routes/eckovation');
/*  ****************************************************************************    */




/*  ***************************** APP MODELS  **********************************    */

/*  ****************************************************************************    */




/*  ************************* APP BASIC UTILITIES *******************************    */
var configs                     = require('./configs/configs.js');
var DbConfigs                   = require('./database/configs.js');
/*  *****************************************************************************    */


var app                         = express();



/*  *************************** APP TRUST PROXIES *******************************    */
//enable only particular ips(of nginx servers) under trust proxy for logging client ip address

/*  *****************************************************************************    */





/*  **************************** APP MORGAN LOGER *******************************    */
var morganLogFullFileName     = getMorganLoggerFileName();
var accessLogStream           = fs.createWriteStream(morganLogFullFileName, {flags: 'a'});
app.use(morgan('{"remoteAddr":":remote-addr","remoteUser":":remote-user","date":":date","method":":method","url":":url","httpVersion":":http-version","status":":status","resp_cont_len":":res[content-length]","referrer":":referrer","user_agent":":user-agent","response_time":":response-time"}', {stream: accessLogStream}));
/*  ****************************************************************************    */





/*  **************************** APP DATABASE CONNECTION *******************************    */
connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
});
/*  ************************************************************************************    */



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// create application/json parser 
app.use(bodyParser.json());
// create application/x-www-form-urlencoded parser 
app.use(bodyParser.urlencoded({ extended: false }));



/*  ******************************** APP VALIDATORS ***********************************    */
app.use(expressValidator({
  customValidators: {
    isValidMongoId : function(value) {
      var regex = /^[0-9a-f]{24}$/;
      return regex.test(value);
    }
  }
}));

/*  **********************************************************************************    */


app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,content-encoding');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // sets options call as a cache to browser
    res.setHeader('Access-Control-Max-Age', 86400);
    // Pass to next layer of middleware
    next();
});

// we are not using cookies for now
app.use(cookieParser());

// to serve the static files, from the server
app.use(express.static(path.join(__dirname, 'web')));

app.use(cors());

// disable etag for request caching
app.set('etag',false);

// app.use(compression());


/*  ***************************** APP ROUTE BINDING *********************************    */
app.use('/chapter',chapter);
app.use('/topic',topic);
app.use('/user',user);
app.use('/userchapter',userchapter);
app.use('/userchaptertopic',userchaptertopic);
app.use('/eckovation',eckovation);
/*  **********************************************************************************    */


app.get('/proxy.html', function(req,res,next){
  res.set("KeepAlive", false);
  res.status(200).send('<!DOCTYPE HTML><script src="//cdn.eckovation.com/xdomain/xdomain.min.js" data-master="http://*.pathshala.eckovation.com"></script>');
  return;
});

/*  ******************************** ERROR HANDLER ***********************************    */
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500).json({});
});
/*  **********************************************************************************    */


function getMorganLoggerFileName(){
  var morganLogDirectory= configs.MORGAN_LOG_DIRECTORY;
  var current_date      = new Date();
  var file_date_ext     = current_date.getFullYear()+'_'+(current_date.getMonth()+1)+'_'+current_date.getDate();
  var fullFileName      = morganLogDirectory + '/access-'+file_date_ext+'.log';
  return fullFileName;
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      },
      poolSize : DbConfigs.MONGO_POOLSIZE
    }
  };
  if(!DbConfigs.MONGO_UNAME || DbConfigs.MONGO_UNAME == ""){
    console.log('[[[[[[[    NO MONGO_UNAME FOUND IN DBCONFIGS, WILL CONNNECT WITHOUT UNAME    ]]]]]]]]]');
    return mongoose.connect("mongodb://"+DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT+"/"+DbConfigs.DB_NAME,options);
  }
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string, options);
}

module.exports = app;
