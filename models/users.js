var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var user = new mongoose.Schema(
	{
		identifier 		: { type : String, required : true },
		server_id     : { type : ObjectId, required : true },
		gid           : { type : String, required : true },
		act 				  : { type : Boolean, required : true },
		name 					: { type : String, required : false },
		email 				: { type : String, required : false },
		img_url   		: String,
		role          : Number,
		is_admin      : String,
		desc      		: String
	},
	{ 
		timestamps : true
	}
);

user.statics = {
	GROUP_AUTH : {
		ADMIN     : '5999',
		NOT_ADMIN : '8999'
	}
};

user.index({identifier:1,act:1});
user.index({identifier:1,gid:1,act:1});

mongoose.model('User',user);