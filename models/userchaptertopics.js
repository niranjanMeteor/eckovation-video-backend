var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var userchaptertopic = new mongoose.Schema(
	{
		user_id 			    : { type : ObjectId, required : true },
		user_chap_id 			: { type : ObjectId, required : true },
		topic_id 			    : { type : ObjectId, required : true },
		status            : String,
		tim               : Number
	},
	{ 
		timestamps : true
	}
);

userchaptertopic.statics = {
	STATUS : {
		UNTOUCHED : 1,
		SEEN  : 3
	}
};

userchaptertopic.index({user_id:1,user_chap_id:1});
userchaptertopic.index({user_id:1,user_chap_id:1,topic_id:1});

mongoose.model('UserChapterTopic',userchaptertopic);