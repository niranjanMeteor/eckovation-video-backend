var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
 
var pluginpaidinfo = new mongoose.Schema(
	{
		plugin_id     : { type : String, required : true },
		gid           : { type : String, required : true },
		act           : { type : Boolean, required : true }
	},
	{ 
		timestamps : true
	}
);

pluginpaidinfo.index({plugin_id:1,gid:1,act:1});

mongoose.model('PluginPaidInfo',pluginpaidinfo);