var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var userchapter = new mongoose.Schema(
	{
		user_id 			    : { type : ObjectId, required : true },
		group_chap_id 		: { type : ObjectId, required : true },
		status            : String,
		tim               : Number
	},
	{ 
		timestamps : true
	}
);

userchapter.statics = {
	STATUS : {
		UNTOUCHED : 1,
		PARTIAL   : 2,
		COMPLETE  : 3
	}
};

userchapter.index({user_id:1,group_chap_id:1});

mongoose.model('UserChapter',userchapter);