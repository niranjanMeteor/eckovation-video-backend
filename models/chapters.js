var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var chapter = new mongoose.Schema(
	{
		name 			        : { type : String, required : true },
		img_url		        : String,
		desc              : String,
		tim               : Number
	},
	{ 
		timestamps : true
	}
);

chapter.index({name:1});

mongoose.model('Chapter',chapter);