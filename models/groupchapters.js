var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var groupchapter = new mongoose.Schema(
	{
		gid 			        : { type : String, required : true },
		chap_id 			    : { type : ObjectId, required : true },
		act               : { type : Boolean,  required : true },
		chap_name         : String,
		tim               : Number
	},
	{ 
		timestamps : true
	}
);

groupchapter.index({gid:1,chap_name:1,act:1});
groupchapter.index({gid:1,chap_id:1,act:1});

mongoose.model('GroupChapter',groupchapter);