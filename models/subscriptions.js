var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var subscription = new mongoose.Schema(
	{
		plugin_id     		: { type : String, required : true },
		group_id      		: { type : String, required : true },
		identifier 				: { type : String, required : true },
		package_id    		: { type : String, required : true },
		name              : { type : String, required : true },
		price 		        : { type : String, required : true },
		amount_paid 			: { type : String, required : true },
		order_id      		: { type : String, required : true },
		start_time   		  : { type : String, required : true },
		end_time          : { type : String, required : true },
		act               : { type : Boolean, required : true },
		access_token      : { type : String, required : false },
	},
	{ 
		timestamps : true
	}
);

subscription.index({group_id:1,identifier:1,act:1});
subscription.index({plugin_id:1,group_id:1,identifier:1,package_id:1,act:1});

mongoose.model('Subscription',subscription);