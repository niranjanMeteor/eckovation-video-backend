var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var topic = new mongoose.Schema(
	{
		chap_id 			    : { type : String, required : true },
		name 			        : { type : String, required : true },
		act               : { type : Boolean,  required : true },
		video_url         : String,
		img_url		        : String,
		pdf_url           : String,
		desc              : String,
		tim               : Number
	},
	{ 
		timestamps : true
	}
);

topic.index({chap_id:1,act:1});
topic.index({chap_id:1,name:1,act:1});
topic.index({tim:1});

mongoose.model('Topic',topic);