var log4js_configs = {

	LOG_DIR_ROUTES_USER   													: '/var/log/videoplugin/routes/user',
	LOG_DIR_ROUTES_USERCHAPTER   										: '/var/log/videoplugin/routes/userchapter',
	LOG_DIR_ROUTES_USERCHAPTERTOPIC   							: '/var/log/videoplugin/routes/userchaptertopic',
	LOG_DIR_ROUTES_CHAPTER   												: '/var/log/videoplugin/routes/chapter',
	LOG_DIR_ROUTES_TOPIC   													: '/var/log/videoplugin/routes/topic',
	LOG_DIR_ROUTES_ECKOVATION  										  : '/var/log/videoplugin/routes/eckovation',
	CATEGORY_ROUTES_USER  													: 'User',
	CATEGORY_ROUTES_USERCHAPTER 										: 'UserChapter',
	CATEGORY_ROUTES_USERCHAPTERTOPIC  							: 'UserChapterTopic',
	CATEGORY_ROUTES_CHAPTER  							          : 'Chapter',
	CATEGORY_ROUTES_TOPIC  							            : 'Topic',
	CATEGORY_ROUTES_ECKOVATION                      : 'Eckovation'

};

module.exports = log4js_configs;

