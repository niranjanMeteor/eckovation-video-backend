var log4js              	     = require('log4js');
var Log4jsConfigs         	     = require('./log4js_configs.js');

var current_date        	     = new Date();
var file_date_ext       	     = current_date.getFullYear()+'_'+(current_date.getMonth()+1)+'_'+current_date.getDate();

var routesUserErrorLog              = Log4jsConfigs.LOG_DIR_ROUTES_USER+'/err_'+file_date_ext+'.log';
var routesUserChapterErrorLog       = Log4jsConfigs.LOG_DIR_ROUTES_USERCHAPTER+'/err_'+file_date_ext+'.log';
var routesUserChapterTopicErrorLog  = Log4jsConfigs.LOG_DIR_ROUTES_USERCHAPTERTOPIC+'/err_'+file_date_ext+'.log';
var routesChapterErrorLog           = Log4jsConfigs.LOG_DIR_ROUTES_CHAPTER+'/err_'+file_date_ext+'.log';
var routesTopicErrorLog             = Log4jsConfigs.LOG_DIR_ROUTES_TOPIC+'/err_'+file_date_ext+'.log';
var routesEckovationLog             = Log4jsConfigs.LOG_DIR_ROUTES_ECKOVATION+'/info_'+file_date_ext+'.log';

log4js.configure({
  appenders: [
    { type: 'file', filename: routesUserErrorLog,              category: Log4jsConfigs.CATEGORY_ROUTES_USER},
    { type: 'file', filename: routesUserChapterErrorLog,       category: Log4jsConfigs.CATEGORY_ROUTES_USERCHAPTER},
    { type: 'file', filename: routesUserChapterTopicErrorLog,  category: Log4jsConfigs.CATEGORY_ROUTES_USERCHAPTERTOPIC},
  	{ type: 'file', filename: routesChapterErrorLog,           category: Log4jsConfigs.CATEGORY_ROUTES_CHAPTER},
  	{ type: 'file', filename: routesTopicErrorLog,             category: Log4jsConfigs.CATEGORY_ROUTES_TOPIC},
  	{ type: 'file', filename: routesEckovationLog,             category: Log4jsConfigs.CATEGORY_ROUTES_ECKOVATION}
  ]
});

module.exports = log4js;
