var jwt    									= require('jsonwebtoken');
var mongoose 							  = require('mongoose');
var configs                 = require('../configs/configs.js');

require('../models/subscriptions.js');
var Subscription 					 = mongoose.model('Subscription');

exports.getAT = function(object_for_token){
	var expiryTime         = configs.JWT_ACCESS_TOKEN_EXPIRY_TIME;
	var private_key        = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
	object_for_token.hcode = randomNumberOfLength(configs.JWT_ACCESS_TOKEN_HCODE_LENGTH);
	return jwt.sign(
		object_for_token, 
		private_key, 
		{
	  	expiresIn: expiryTime
		}
	);
};

exports.getSubscriptionAT = function(object_for_token, identifier, group_id, tim, cb){
	Subscription.findOne({
		identifier : identifier,
		group_id    : group_id,
		act        : true
	},function(err, subscription){
		if(err){
			return cb(err,null);
		}
		if(!subscription){
			return cb("USER_SUBSCRIPTION_REQUIRED",null);
		}
		if(!subscription.start_time || !subscription.end_time){
			return cb("Subscription_Not_Valid",null);
		}
		var expiryTime         = parseInt(((subscription.end_time - tim) / 1000).toFixed());
		console.log('printing expiry calculated');
		console.log(expiryTime);
		if(subscription.access_token){
			if(expiryTime <= 0){
				return cb("USER_SUBSCRIPTION_EXPIRED",null);
			} else {
				return cb(null,subscription.access_token);
			}
		}
		console.log('will generate first time token');
		var private_key        = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		var access_token = jwt.sign(
			object_for_token, 
			private_key, 
			{
		  	expiresIn: expiryTime
			}
		);
		Subscription.update({
			_id : subscription._id
		},{
			access_token : access_token
		},function(err,subscription_updated){
			if(err){
				return cb(err,null);
			}
			return cb(null, access_token);
		});
	});
};

function randomNumberOfLength(length) {
  return Math.floor(Math.pow(10, length-1) + Math.random() * (Math.pow(10, length) - Math.pow(10, length-1) - 1));
}