require('shelljs/global');
var configs           = require('./configs/configs.js');

var command = 'service '+configs.VIDEOPLUGIN_SERVER_SERVICE_NAME+' stop';
exec(command, function(status, output) {
  console.log('Exit status:', status);
  console.log('Program output:', output);
});