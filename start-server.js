require('shelljs/global');
var app 												= require('express')();
var fs 													= require('fs');
var moment											= require('moment');
var configs 										= require('./configs/configs.js');

var dirname 										= __dirname;
var server_conf_filename 				= configs.VIDEOPLUGIN_SERVER_SERVICE_NAME+'.conf'
var server_conf_full_filename 	= dirname+'/'+server_conf_filename;
var logDir 											= configs.VIDEOPLUGIN_SERVER_LOG_DIR;
var logFile 										= logDir+'/VIDEOPLUGIN-'+moment().format()+'.log';
var VideoPluginHeapSize         = configs.VIDEOPLUGIN_SERVER_HEAP_SIZE_MAX;

rm('-rf',server_conf_full_filename);

var data = 'limit nofile 150000 150000\n\n';
    data += 'start on runlevel [2345]\n\n';
		data += 'setuid '+configs.VIDEOPLUGIN_SERVER_SERVICE_USER+'\n\n';
		data += 'respawn\n\n';

if(app.get('env') !== 'debug') {
	data += 'pre-start script\n';
	data += ' exec /usr/bin/node '+dirname+'/mailnotify/server_restart.js 2>&1\n';
	data += 'end script\n\n';
}

data += 'exec /usr/bin/node --max_old_space_size='+VideoPluginHeapSize+' '+dirname+'/bin/www >> '+logFile+' 2>&1\n\n';

if(app.get('env') !== 'debug') {
	data += 'post-stop script\n';
	data += ' exec /usr/bin/node '+dirname+'/mailnotify/server_crash.js 2>&1\n';
	data += 'end script\n\n';
}

fs.appendFile(server_conf_full_filename, data, function(err){
	if (err) {
		console.trace(err);
		return;
	}
	mkdir('-p', logDir);

	// TO BE THOUGHT WHETEHR PERMISSONS LESSAR THATN 777 CAN BE GIVEN
	chmod(777, logDir);

	cp('-f',server_conf_full_filename, '/etc/init/'+server_conf_filename);
	var command = 'service '+configs.VIDEOPLUGIN_SERVER_SERVICE_NAME+' start';
	exec(command, function(status, output) {
	  console.log('Exit status:', status);
	  console.log('Program output:', output);
	});
});