var express 							= require('express');
var mongoose 							= require('mongoose');
var jwt    								= require('jsonwebtoken');

var configs					      = require('../configs/configs.js');
var HttpStatuses					= require('../global/http_api_return_codes.js');
var ApiReturnHandlers			= require('../global/api_return_handlers.js');
var errorCodes 						= require('../global/error_codes.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');
var AuthSecurity      		= require('../security/auth_tokens.js');
var RootServerApi      		= require('../server/apicalls.js');

require('../models/subscriptions.js');
var Subscription 					= mongoose.model('Subscription');

var sendError 						= ApiReturnHandlers.sendError;
var sendSuccess 					= ApiReturnHandlers.sendSuccess;
var router 								= express.Router();
var logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_ECKOVATION);


//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
	var tim           = new Date().getTime();
	var parameters    = req.body.parameters;
	if(!parameters){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"paramete_not_found","p":req.body});
	  return sendError(res,"Bad Request","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	parameters = parameters.trim();
	if(!parameters){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"paramete_not_found_after_trimming","p":req.body});
	  return sendError(res,"Bad Request","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
  jwt.verify(parameters, configs.ECKOVATION_API_KEY, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate Request.","request_auth_failure",HttpStatuses.FORBIDDEN);  
    } 
    req.tim     = tim;
    req.decoded = decoded;
    next();
  });
});

router.post('/eckovationcallback', function(req, res, next){

	if(!isValidParams(req.decoded)){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	
	var plugin_id    						= req.decoded.PLUGIN_ID;
	var group_id     						= req.decoded.GROUP_ID;
	var identifier   						= req.decoded.IDENTIFIER;
	var package_id   						= req.decoded.PACKAGE_ID;
	var package_name 						= req.decoded.PACKAGE_NAME;
	var package_price						= req.decoded.PACKAGE_PRICE;
	var promo_code              = req.decoded.PROMO_CODE;
	var final_amount_paid       = req.decoded.ORDER_AMOUNT;
	var subscription_start_time = req.decoded.START_TIME;
	var subscription_end_time   = req.decoded.END_TIME; 
	var order_id                = req.decoded.ORDER_ID;
	var tim                     = req.decoded.tim;

	Subscription.update({
		plugin_id : plugin_id,
		group_id  : group_id,
		identifier: identifier,
		act       : true
	},{
		act : false
	},{
		multi : true
	},function(err,existing_subscription){
		if(err){
			logger.error({"r":"eckovationcallback","er":err,"p":req.decoded,"msg":"Subscription_updateError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		Subscription.update({
			plugin_id : plugin_id,
			group_id  : group_id,
			identifier: identifier,
			package_id: package_id,
			act       : true
		},{
			$setOnInsert : {
				plugin_id 		: plugin_id,
				group_id  		: group_id,
				identifier    : identifier,
				package_id    : package_id,
				name          : package_name,
				price         : package_price,
				order_id      : order_id,
				amount_paid   : final_amount_paid,
				start_time    : subscription_start_time,
				end_time      : subscription_end_time,
				act           : true
			}
		},{
			upsert : true,
			setDefaultsOnInsert: true
		},function(err,subscription_created){
			if(err){
				logger.error({"r":"eckovationcallback","er":err,"p":req.decoded,"msg":"Subscription_Insertion_Error"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			logger.error({"r":"eckovationcallback","msg":"New_Subscription_Created","p":req.decoded});
			return sendSuccess(res,{});
		});
	});
});

function isValidParams(parameters){
	if(!parameters.PLUGIN_ID){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_PLUGIN_ID","p":parameters});
		return false;
	}
	// if(parameters.PLUGIN_ID != configs.PLUGIN_ID){
	// 	logger.error({"r":"eckovationcallback","msg":"invalid_parameters_value_PLUGIN_ID","p":parameters});
	// 	return false;
	// }
	if(!parameters.GROUP_ID){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_GROUP_ID","p":parameters});
		return false;
	}
	if(!parameters.IDENTIFIER){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_IDENTIFIER","p":parameters});
		return false;
	}
	if(!parameters.PACKAGE_ID){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_PACKAGE_ID","p":parameters});
		return false;
	}
	if(!parameters.PACKAGE_NAME){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_PACKAGE_NAME","p":parameters});
		return false;
	}
	if(!parameters.PACKAGE_PRICE){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_PACKAGE_PRICE","p":parameters});
		return false;
	}
	// if(!parameters.PROMO_CODE){
	// 	logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_PROMO_CODE","p":parameters});
	// 	return false;
	// }
	if(!parameters.ORDER_AMOUNT){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_ORDER_AMOUNT","p":parameters});
		return false;
	}
	if(!parameters.START_TIME){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_START_TIME","p":parameters});
		return false;
	}
	if(!parameters.END_TIME){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_END_TIME","p":parameters});
		return false;
	}
	if(!parameters.ORDER_ID){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_ORDER_ID","p":parameters});
		return false;
	}
	return true;
}



module.exports = router;