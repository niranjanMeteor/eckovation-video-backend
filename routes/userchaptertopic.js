var express 							= require('express');
var mongoose 							= require('mongoose');
var jwt    								= require('jsonwebtoken');

var configs					      = require('../configs/configs.js');
var HttpStatuses					= require('../global/http_api_return_codes.js');
var ApiReturnHandlers			= require('../global/api_return_handlers.js');
var errorCodes 						= require('../global/error_codes.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');

require('../models/groupchapters.js');
require('../models/userchapters.js');
require('../models/userchaptertopics.js');
require('../models/topics.js');
require('../models/pluginpaidinfos.js');
var GroupChapter 					= mongoose.model('GroupChapter');
var UserChapter 					= mongoose.model('UserChapter');
var UserChapterTopic 			= mongoose.model('UserChapterTopic');
var Topic 			          = mongoose.model('Topic');
var PluginPaidInfo 				= mongoose.model('PluginPaidInfo');

var sendError 						= ApiReturnHandlers.sendError;
var sendSuccess 					= ApiReturnHandlers.sendSuccess;
var router 								= express.Router();
var logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_USERCHAPTERTOPIC);


//route middleware to verify if plugin in group is paid
router.use(function(req, res, next){
	var tim       = new Date().getTime();
	var token     = req.body.tokn;
	var gid       = req.body.g_id;
	if(!token || token == 'null' || token === "undefined"){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body,"r":"first_middleware","tkn":token});
	  return sendError(res,"Access without token is not authorised","token_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!gid){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"gid_not_found","p":req.body,"r":"first_middleware","tkn":req.token});
	  return sendError(res,"Access without id is not authorised","gid_not_found",HttpStatuses.FORBIDDEN);
	}
	PluginPaidInfo.findOne({
		plugin_id : configs.PLUGIN_ID,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError","r":"first_middleware","tkn":token});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			req.private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			req.private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
		req.tim = tim;
		req.token = token;
	  next();
	});
});

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
	var user_id   = req.body.user_id;
	var gid       = req.body.g_id;
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body,"r":"2nd_middleware","tkn":req.token});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
  jwt.verify(req.token, req.private_key, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err,"r":"second_middleware","tkn":req.token});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
    } 
    if(decoded.id1 != user_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_userid","p":req.body,"r":"second_middleware","tkn":req.token});
	  	return sendError(res,"Access without valid user id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id3 != configs.PLUGIN_ID){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_plugin_id","p":req.body,"r":"second_middleware","tkn":req.token});
	  	return sendError(res,"Access without Valid App id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id4 != gid){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body,"r":"second_middleware","tkn":req.token});
	  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    next();
  });
});


router.post('/g_tpc', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('chap_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"g_tpc","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id    = req.body.user_id.trim();
	var gid        = req.body.g_id.trim();
	var chap_id    = req.body.chap_id.trim();
	var tim        = req.tim;

	GroupChapter.findOne({
		gid     : gid,
		chap_id : chap_id,
		act     : true
	},function(err,groupchapter){
		if(err){
			logger.error({"r":"g_tpc","er":err,"p":req.body,"msg":"GroupChapter_db_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!groupchapter){
			logger.error({"r":"g_tpc","msg":"no GroupChapters Found for gid","p":req.body});
			return sendError(res,"No GroupChapters Found","no_groupchapters_found",HttpStatuses.NO_DATA);
		}
		UserChapter.findOne({
			user_id : user_id,
			group_chap_id : groupchapter._id
		},function(err, userchapter){
			if(err){
				logger.error({"r":"g_tpc","er":err,"p":req.body,"msg":"UserChapter_db_findError"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!userchapter){
				logger.error({"r":"g_tpc","msg":"no_userchapters_found for gid","p":req.body});
				return sendError(res,"No UserChapters Found","no_userchapters_found",HttpStatuses.NO_DATA);
			}
			UserChapterTopic.find({
				user_id : user_id,
				user_chap_id : userchapter._id
			},function(err,userchaptertopics){
				if(err){
					logger.error({"r":"g_tpc","er":err,"p":req.body,"msg":"UserChapterTopic_db_findError"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				var statusByTopic = [];
				for(var i=0; i<userchaptertopics.length; i++){
					statusByTopic[userchaptertopics[i].topic_id+''] = userchaptertopics[i].status;
				}
				Topic.find({
					chap_id : chap_id,
					act     : true
				},{},{
					sort : {
						tim : 1
					}
				},function(err,topics){
					if(err){
						logger.error({"r":"g_tpc","er":err,"p":req.body,"msg":"Topic_db_findError"});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(topics.length == 0){
						logger.error({"r":"g_tpc","msg":"no_topics_found for gid","p":req.body});
						return sendError(res,"No Chapter Topics Found","no_chaptertopics_found",HttpStatuses.NO_DATA);
					}
					var results = [];
					for(var k=0; k<topics.length; k++){
						results[k] = {
							tpc : topics[k],
							st  : (!statusByTopic[topics[k]._id+'']) ? UserChapterTopic.STATUS.UNTOUCHED : statusByTopic[topics[k]._id+'']
						}
					}
					logger.info({"r":"g_tpc","p":req.body});
					return sendSuccess(res,{sub_items:results,chap_name:groupchapter.chap_name});
				});
			});
		});
	});
});

router.post('/open', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('chap_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('tpc_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"open","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id    = req.body.user_id.trim();
	var gid        = req.body.g_id.trim();
	var chap_id    = req.body.chap_id.trim();
	var topic_id   = req.body.tpc_id.trim();
	var tim        = req.tim;

	GroupChapter.findOne({
		gid     : gid,
		chap_id : chap_id,
		act     : true
	},function(err,groupchapter){
		if(err){
			logger.error({"r":"open","er":err,"p":req.body,"msg":"GroupChapter_db_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!groupchapter){
			logger.error({"r":"open","msg":"groupchapter_not_found","p":req.body});
			return sendError(res,"GroupChapter Not Found","groupchapter_not_found",HttpStatuses.BAD_REQUEST);
		}
		UserChapter.findOne({
			user_id       : user_id,
			group_chap_id : groupchapter._id,
		},function(err,userchapter){
			if(err){
				logger.error({"r":"open","er":err,"p":req.body,"msg":"GroupChapter_db_findError"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!userchapter){
				logger.error({"r":"open","msg":"no_userchapters_found","p":req.body});
				return sendError(res,"No UserChapters Found","no_userchapters_found",HttpStatuses.BAD_REQUEST);
			}
			UserChapterTopic.findOne({
				user_id      : user_id,
				user_chap_id : userchapter._id,
				topic_id     : topic_id
			},function(err,userchaptertopic){
				if(err){
					logger.error({"r":"open","er":err,"p":req.body,"msg":"GroupChapter_db_findError"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(userchaptertopic && userchaptertopic.status){
					return sendSuccess(res,{});
				}
				UserChapterTopic.update({
					user_id      : user_id,
					user_chap_id : userchapter._id,
					topic_id     : topic_id
				},{
					$setOnInsert : {
						user_id      : user_id,
						user_chap_id : userchapter._id,
						topic_id     : topic_id,
						status       : UserChapterTopic.STATUS.SEEN
					},
					$set : {
						tim : tim
					}
				},{
					upsert : true,
					setDefaultsOnInsert: true
				},function(err,resp){
					if(err){
						logger.error({"r":"open","er":err,"p":req.body,"msg":"UserChapterTopic_db_findError"});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					checkAndUpdateUserChapter(user_id, chap_id, userchapter._id, function(err,resp){
						if(err){
							logger.error({"r":"open","er":err,"p":req.body,"msg":"checkAndUpdateUserChapter_Error"});
							console.trace(err);
						}
						logger.info({"r":"open","p":req.body});
						return sendSuccess(res,{});
					});
				});
			});
		});
	});
});

function checkAndUpdateUserChapter(user_id, chap_id, user_chap_id, cb){
	Topic.find({
		chap_id : chap_id
	},{
		_id : 1
	},function(err,topics){
		if(err){
			return cb(err,null);
		}
		var topicIds = [];
		for(var i=0; i<topics.length; i++){
			topicIds.push(topics[i]._id+'');
		}
		UserChapterTopic.count({
			user_id : user_id,
			user_chap_id : user_chap_id,
			topic_id : { $in : topicIds },
			status : UserChapterTopic.STATUS.SEEN
		},function(err,total_view_count){
			if(err){
				return cb(err,null);
			}
			if(total_view_count < topics.length){
				return 	cb(null,false);
			}
			UserChapter.update({
				_id : user_chap_id
			},{
				status : UserChapter.STATUS.COMPLETE
			},function(err,resp){
				if(err){
					return cb(err,null);
				}
				return cb(null,true);
			});
		});
	});
}

module.exports = router;