var express 							= require('express');
var mongoose 							= require('mongoose');
var jwt    								= require('jsonwebtoken');

var configs					      = require('../configs/configs.js');
var HttpStatuses					= require('../global/http_api_return_codes.js');
var ApiReturnHandlers			= require('../global/api_return_handlers.js');
var errorCodes 						= require('../global/error_codes.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');

require('../models/chapters.js');
require('../models/groupchapters.js');
require('../models/users.js');
require('../models/userchapters.js');
require('../models/pluginpaidinfos.js');
var Chapter 					    = mongoose.model('Chapter');
var GroupChapter 					= mongoose.model('GroupChapter');
var User 									= mongoose.model('User');
var UserChapter 					= mongoose.model('UserChapter');
var PluginPaidInfo 				= mongoose.model('PluginPaidInfo');

var sendError 						= ApiReturnHandlers.sendError;
var sendSuccess 					= ApiReturnHandlers.sendSuccess;
var router 								= express.Router();
var logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_USERCHAPTER);


//route middleware to verify if plugin in group is paid
router.use(function(req, res, next){
	var tim       = new Date().getTime();
	var token     = req.body.tokn;
	var gid       = req.body.g_id;
	if(!token || token == 'null' || token === "undefined"){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body,"r":"first_middleware","tkn":token});
	  return sendError(res,"Access without token is not authorised","token_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!gid){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"gid_not_found","p":req.body,"r":"first_middleware","tkn":req.token});
	  return sendError(res,"Access without id is not authorised","gid_not_found",HttpStatuses.FORBIDDEN);
	}
	PluginPaidInfo.findOne({
		plugin_id : configs.PLUGIN_ID,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError","r":"first_middleware","tkn":token});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			req.private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			req.private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
		req.tim = tim;
		req.token = token;
	  next();
	});
});

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
	var user_id   = req.body.user_id;
	var gid       = req.body.g_id;
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body,"r":"2nd_middleware","tkn":req.token});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
  jwt.verify(req.token, req.private_key, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err,"r":"second_middleware","tkn":req.token});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
    } 
    if(decoded.id1 != user_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_userid","p":req.body,"r":"second_middleware","tkn":req.token});
	  	return sendError(res,"Access without valid user id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id3 != configs.PLUGIN_ID){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_plugin_id","p":req.body,"r":"second_middleware","tkn":req.token});
	  	return sendError(res,"Access without Valid App id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id4 != gid){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body,"r":"second_middleware","tkn":req.token});
	  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    next();
  });
});

router.post('/g_uchp', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"g_uchp","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id    = req.body.user_id.trim();
	var gid        = req.body.g_id.trim();
	var tim        = req.tim;

	GroupChapter.find({
		gid : gid,
		act : true
	},function(err,groupchapters){
		if(err){
			logger.error({"r":"g_uchp","er":err,"p":req.body,"msg":"GroupChapter_db_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		var total_group_chapters = groupchapters.length;
		if(total_group_chapters == 0){
			logger.error({"r":"g_uchp","msg":"no GroupChapters Found for gid","p":req.body});
			return sendError(res,"No GroupChapters Found","no_groupchapters_found",HttpStatuses.NO_DATA);
		}
		var groupchapters_ids = [];
		var chapter_ids       = [];
		for(var i=0; i<total_group_chapters; i++){
			groupchapters_ids.push(groupchapters[i]._id+'');
			chapter_ids.push(groupchapters[i].chap_id+'');
		}
		Chapter.find({
			_id : { $in : chapter_ids}
		},{
			name : 1,
			img_url : 1,
			desc : 1
		},function(err,chapters){
			if(err){
				logger.error({"r":"g_uchp","er":err,"p":req.body,"msg":"Chapter_db_findError"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			chapter_ids = null;
			var total_chapters = chapters.length;
			if(total_chapters == 0){
				logger.error({"r":"g_uchp","msg":"no Chapters Found for gid","p":req.body});
				return sendError(res,"No GroupChapters Found","no_chapters_found",HttpStatuses.NO_DATA);
			}
			var chapterById = [];
			for(var k=0; k<total_chapters; k++){
				chapterById[chapters[k]._id+''] = chapters[k];
			}
			chapters = null;
			UserChapter.find({
				user_id       : user_id,
				group_chap_id : { $in : groupchapters_ids}
			},{
				group_chap_id : 1,
				status : 1
			},function(err,groupchapters_status){
				if(err){
					logger.error({"r":"g_uchp","er":err,"p":req.body,"msg":"UserChapter_db_findError"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				groupchapters_ids = null;
				var statusById = [];
				for(var j=0; j<groupchapters_status.length; j++){
					statusById[groupchapters_status[j].group_chap_id+''] = groupchapters_status[j].status;
				}
				var results = [];
				var status;
				for(var m=0; m<total_group_chapters; m++){
					status = (!statusById[groupchapters[m]._id]) ? UserChapter.STATUS.UNTOUCHED : statusById[groupchapters[m]._id];
					results[m] = {
						chp : chapterById[groupchapters[m].chap_id],
						st  : status
					}
				}
				logger.info({"r":"g_uchp","p":req.body});
				return sendSuccess(res,{items:results});
			});
		});
	});
});

router.post('/open', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('chap_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"open","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id    = req.body.user_id.trim();
	var chap_id    = req.body.chap_id.trim();
	var gid        = req.body.g_id.trim();
	var tim        = req.tim;

	GroupChapter.findOne({
		gid : gid,
		chap_id : chap_id,
		act : true
	},function(err,groupchapter){
		if(err){
			logger.error({"r":"open","er":err,"p":req.body,"msg":"GroupChapter_db_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!groupchapter){
			logger.error({"r":"open","msg":"groupchapter_not_found","p":req.body});
			return sendError(res,"GroupChapter Not Found","groupchapter_not_found",HttpStatuses.BAD_REQUEST);
		}
		UserChapter.findOne({
			user_id       : user_id,
			group_chap_id : groupchapter._id,
		},function(err,userchapter){
			if(err){
				logger.error({"r":"open","er":err,"p":req.body,"msg":"GroupChapter_db_findError"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(userchapter && userchapter.status){
				return sendSuccess(res,{});
			}
			UserChapter.update({
				user_id       : user_id,
				group_chap_id : groupchapter._id
			},{
				$setOnInsert : {
					user_id       : user_id,
					group_chap_id : groupchapter._id,
					status        : UserChapter.STATUS.PARTIAL
				},
				$set : {
					tim : tim
				}
			},{
				upsert : true,
				setDefaultsOnInsert: true
			},function(err,resp){
				if(err){
					logger.error({"r":"open","er":err,"p":req.body,"msg":"GroupChapter_db_findError"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				logger.info({"r":"open","p":req.body});
				return sendSuccess(res,{});
			});
		});
	});
});

module.exports = router;