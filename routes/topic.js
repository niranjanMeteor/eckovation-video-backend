var express 							= require('express');
var mongoose 							= require('mongoose');
var jwt    								= require('jsonwebtoken');

var configs					      = require('../configs/configs.js');
var HttpStatuses					= require('../global/http_api_return_codes.js');
var ApiReturnHandlers			= require('../global/api_return_handlers.js');
var errorCodes 						= require('../global/error_codes.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');
var RootServerApi      		= require('../server/apicalls.js');

require('../models/chapters.js');
require('../models/topics.js');
require('../models/groupchapters.js');
require('../models/users.js');
require('../models/pluginpaidinfos.js');
var Chapter 					    = mongoose.model('Chapter');
var Topic 					      = mongoose.model('Topic');
var GroupChapter 					= mongoose.model('GroupChapter');
var User 									= mongoose.model('User');
var PluginPaidInfo 				= mongoose.model('PluginPaidInfo');

var sendError 						= ApiReturnHandlers.sendError;
var sendSuccess 					= ApiReturnHandlers.sendSuccess;
var router 								= express.Router();
var logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_TOPIC);


//route middleware to verify if plugin in group is paid
router.use(function(req, res, next){
	var tim       = new Date().getTime();
	var token     = req.body.tokn;
	var gid       = req.body.g_id;
	if(!token || token == 'null' || token === "undefined"){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body,"r":"first_middleware","tkn":token});
	  return sendError(res,"Access without token is not authorised","token_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!gid){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"gid_not_found","p":req.body,"r":"first_middleware","tkn":req.token});
	  return sendError(res,"Access without id is not authorised","gid_not_found",HttpStatuses.FORBIDDEN);
	}
	PluginPaidInfo.findOne({
		plugin_id : configs.PLUGIN_ID,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError","r":"first_middleware","tkn":token});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			req.private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			req.private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
		req.tim = tim;
		req.token = token;
	  next();
	});
});

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
	var user_id   = req.body.user_id;
	var gid       = req.body.g_id;
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body,"r":"2nd_middleware","tkn":req.token});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
  jwt.verify(req.token, req.private_key, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err,"tkn":req.token});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
    } 
    if(decoded.id1 != user_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_userid","p":req.body,"tkn":req.token});
	  	return sendError(res,"Access without valid user id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id3 != configs.PLUGIN_ID){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_plugin_id","p":req.body,"tkn":req.token});
	  	return sendError(res,"Access without Valid App id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id4 != gid){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body,"tkn":req.token});
	  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    User.findOne({
    	_id : user_id,
			act : true,
		},function(err,user){
			if(err){
				logger.error({"url":req.originalUrl,"msg":"user_find_failed","er":err,"p":req.body,"tkn":req.token});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!user){
				logger.info({"url":req.originalUrl,"msg":"user_already_exists","p":req.body,"tkn":req.token});
				return sendSuccess(res,{ id : user._id, if_new : 0, srv_id : user.server_id, gid : user.gid });
			}
			RootServerApi.checkIfAdmin(user.identifier, function(err,resp){
				if(err){
					logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_err","er":err,"p":req.body,"tkn":req.token});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!resp.success){
					logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_failed","p":req.body,"tkn":req.token});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!resp.is_admin){
					logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_UserIsNotGroupAdmin","p":req.body,"tkn":req.token});
					return sendError(res,"unauthorised","unauthorised",HttpStatuses.UNAUTHORIZED);
				}
		    next();
			});
		});
  });
});


router.post('/ad', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('cpt_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('nm',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('v_url',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('p_url',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('i_url',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('desc',errorCodes.invalid_parameters[1]).optional();

	if(req.validationErrors()) { 
		logger.error({"r":"g_uchp","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id    = req.body.user_id.trim();
	var gid        = req.body.g_id.trim();
	var chap_id    = req.body.cpt_id.trim();
	var topic_name = req.body.nm.trim();
	var video_url  = req.body.v_url;
	var img_url    = (req.body.i_url) ? req.body.i_url : "";
	var pdf_url    = (req.body.p_url) ? req.body.p_url : "";
	var desc       = (req.body.desc) ? req.body.desc : "";
	var tim        = req.tim;

	GroupChapter.findOne({
		gid      : gid,
		chap_id  : chap_id,
		act      : true 
	},function(err,groupchapter){
		if(err){
			logger.error({"r":"ad","msg":"GroupChapter_Dberr","er":err,"p":req.body});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!groupchapter){
			logger.error({"r":"ad","msg":"chapter_not_found_in_group","p":req.body});
			return sendError(res,"Chapter Not Found","chapter_not_found",HttpStatuses.BAD_REQUEST);
		}
		Topic.findOne({
			chap_id : chap_id,
			name    : topic_name,
			act     : true
		},function(err,topic_exists){
			if(err){
				logger.error({"r":"ad","msg":"Topic_find_Dberr","er":err,"p":req.body});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(topic_exists){
				logger.error({"r":"ad","msg":"topic_name_for_chapter_already_exists","p":req.body});
				return sendError(res,"Topic Name Already Exists","topic_name_already_exists",HttpStatuses.BAD_REQUEST);
			}
			var new_chapter_topic = new Topic({
				chap_id   : chap_id,
				name      : topic_name,
				video_url : video_url,
				pdf_url   : pdf_url,
				img_url   : img_url,
				desc      : desc,
				act       : true,
				tim       : tim
			});
			new_chapter_topic.save(function(err,new_chapter_topic_created){
				if(err){
					logger.error({"r":"ad","msg":"new_chapter_topic_save_Dberr","er":err,"p":req.body});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				logger.info({"r":"ad","p":req.body});
				return sendSuccess(res,{tp_id : new_chapter_topic_created._id, nm : topic_name, desc : desc, i_url : img_url, v_url : video_url, p_url : pdf_url});
			});
		});
	});
});

router.post('/ed', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('cpt_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('tpc_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('nm',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('v_url',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('p_url',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('i_url',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('desc',errorCodes.invalid_parameters[1]).optional();

	if(req.validationErrors()) { 
		logger.error({"r":"ed","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id    = req.body.user_id.trim();
	var gid        = req.body.g_id.trim();
	var chap_id    = req.body.cpt_id.trim();
	var topic_id   = req.body.tpc_id.trim();
	var toEdit     = {};
	var topic_name, video_url, img_url, video_url = "";

	if(req.body.nm){
		topic_name   = req.body.nm.trim();
		toEdit.name  = topic_name;
	}
	if(req.body.v_url){
		video_url   = req.body.v_url.trim();
		toEdit.video_url = video_url;
	}
	if(req.body.i_url){
		img_url   = req.body.i_url.trim();
		toEdit.img_url = img_url;
	}
	if(req.body.p_url){
		pdf_url   = req.body.p_url.trim();
		toEdit.pdf_url = pdf_url;
	}
	if(req.body.desc){
		desc   = req.body.desc.trim();
		toEdit.desc = desc;
	}
	var tim        = req.tim;

	if(!topic_name && !img_url && !desc && !video_url && !pdf_url){
		logger.error({"r":"ad","msg":"NO_Params_for_Data_Modification","p":req.body});
		return sendError(res,"No Data In Input To Edit","no_data_to_edit",HttpStatuses.NO_INPUT_DATA);
	}

	GroupChapter.findOne({
		gid      : gid,
		chap_id  : chap_id,
		act      : true 
	},function(err,groupchapter){
		if(err){
			logger.error({"r":"ed","msg":"GroupChapter_Dberr","er":err,"p":req.body});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!groupchapter){
			logger.error({"r":"ed","msg":"chapter_not_found_in_group","p":req.body});
			return sendError(res,"Chapter Not Found","chapter_not_found",HttpStatuses.BAD_REQUEST);
		}
		Topic.findOne({
			_id     : topic_id,
			chap_id : chap_id,
			act     : true
		},function(err,topic_exists){
			if(err){
				logger.error({"r":"ed","msg":"Topic_find_Dberr","er":err,"p":req.body});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!topic_exists){
				logger.error({"r":"ed","msg":"chapter_topic_not_found","p":req.body});
				return sendError(res,"Topic Not Found","topic_not_found",HttpStatuses.BAD_REQUEST);
			}
			Topic.update({
				_id : topic_id
			},toEdit,function(){
				if(err){
					logger.error({"r":"ed","msg":"Topic_update_Dberr","er":err,"p":req.body});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				logger.info({"r":"ed","p":req.body});
				return sendSuccess(res,{});
			});
		});
	});
});

router.post('/dl', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('cpt_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('tpc_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"dl","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id    = req.body.user_id.trim();
	var gid        = req.body.g_id.trim();
	var chap_id    = req.body.cpt_id.trim();
	var topic_id   = req.body.tpc_id.trim();
	var tim        = req.tim;

	GroupChapter.findOne({
		gid      : gid,
		chap_id  : chap_id,
		act      : true 
	},function(err,groupchapter){
		if(err){
			logger.error({"r":"dl","msg":"GroupChapter_Dberr","er":err,"p":req.body});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!groupchapter){
			logger.error({"r":"dl","msg":"chapter_not_found_in_group","p":req.body});
			return sendError(res,"Chapter Not Found","chapter_not_found",HttpStatuses.BAD_REQUEST);
		}
		Topic.findOne({
			_id     : topic_id,
			chap_id : chap_id,
			act     : true
		},function(err,topic_exists){
			if(err){
				logger.error({"r":"dl","msg":"Topic_find_Dberr","er":err,"p":req.body});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!topic_exists){
				logger.error({"r":"dl","msg":"chapter_topic_not_found","p":req.body});
				return sendError(res,"Topic Not Found","topic_not_found",HttpStatuses.BAD_REQUEST);
			}
			Topic.update({
				_id : topic_id
			},{
				act : false
			},function(){
				if(err){
					logger.error({"r":"dl","msg":"Topic_update_Dberr","er":err,"p":req.body});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				logger.info({"r":"dl","p":req.body});
				return sendSuccess(res,{});
			});
		});
	});
});

module.exports = router;