var express 							= require('express');
var mongoose 							= require('mongoose');
var jwt    								= require('jsonwebtoken');

var configs					      = require('../configs/configs.js');
var HttpStatuses					= require('../global/http_api_return_codes.js');
var ApiReturnHandlers			= require('../global/api_return_handlers.js');
var errorCodes 						= require('../global/error_codes.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');
var AuthSecurity      		= require('../security/auth_tokens.js');
var RootServerApi      		= require('../server/apicalls.js');

require('../models/users.js');
require('../models/pluginpaidinfos.js');
require('../models/usertokens.js');
var User 									= mongoose.model('User');
var PluginPaidInfo 				= mongoose.model('PluginPaidInfo');
var UserToken 						= mongoose.model('UserToken');

var sendError 						= ApiReturnHandlers.sendError;
var sendSuccess 					= ApiReturnHandlers.sendSuccess;
var router 								= express.Router();
var logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_USER);
var VALID_GROUP_AUTH      = [User.GROUP_AUTH.ADMIN,User.GROUP_AUTH.NOT_ADMIN];

router.post('/register', function(req, res, next){
	req.checkBody('user_idnt',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('i_adm',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"register","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_identifier   = req.body.user_idnt.trim();
	var is_admin          = req.body.i_adm.trim();
	var tim         			= new Date().getTime();

	if(VALID_GROUP_AUTH.indexOf(is_admin) < 0){
		logger.error({"r":"register","msg":"invalid_value_for_is_admin","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	User.findOne({
		act : true,
		identifier : user_identifier
	},function(err,user){
		if(err){
			logger.error({"r":"register","msg":"user_find_failed","er":err,"p":req.body});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(user){
			if(is_admin == VALID_GROUP_AUTH.NOT_ADMIN){
				logger.info({"r":"register","msg":"user_already_exists","p":req.body});
				return sendSuccess(res,{ id : user._id, if_new : 0, srv_id : user.server_id, gid : user.gid });
			}
			RootServerApi.checkIfAdmin(user_identifier, function(err,resp){
				if(err){
					logger.error({"r":"register","msg":"rootServer_checkIfAdmin_err","er":err,"p":req.body});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!resp.success){
					logger.error({"r":"register","msg":"rootServer_checkIfAdmin_failed","p":req.body});
					return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
				}
				return sendSuccess(res,{id : user._id, if_new : 0, srv_id : user.server_id, gid : user.gid, is_admin : resp.is_admin});
			});
		} else {
			RootServerApi.verifyIdentifier(user_identifier, function(err,resp){
				if(err){
					logger.error({"r":"register","msg":"rootServer_verifyIdentifier_err","er":err,"p":req.body});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!resp.success){
					logger.error({"r":"register","msg":"rootServer_verifyIdentifier_failed","p":req.body});
					return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
				}
				var data = resp.data;
				User.update({
					act : true,
					identifier : user_identifier
				},{
					$setOnInsert : {
						act  : true,
						identifier : user_identifier,
						server_id  : data.srv_id,
						name       : data.name,
						img_url    : data.ppic,
						role       : data.role,
						gid        : data.gid
					}
				},{
					upsert : true,
					setDefaultsOnInsert: true
				},function(err,created_user){
					if(err){
						logger.error({"r":"register","msg":"user_token_update_failed","er":err,"p":req.body});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					User.findOne({
						act : true,
						identifier : user_identifier
					},function(err,newly_created_user){
						if(err){
							logger.error({"r":"register","msg":"user_find_failed_after_create","er":err,"p":req.body});
							console.trace(err);
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						if(!newly_created_user){
							logger.error({"r":"register","msg":"user_not_found","p":req.body});
							return sendError(res,"Invalid Parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
						}
						return sendSuccess(res,{id : newly_created_user._id, if_new : 1, srv_id : data.srv_id, gid : data.gid});
					});
				});
			});
		}
	});
});

router.post('/init', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('user_idnt',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('srv_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"init","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id     			= req.body.user_id.trim();
	var server_id     		= req.body.srv_id.trim();
	var group_id     		  = req.body.g_id.trim();
	var user_identifier   = req.body.user_idnt.trim();
	var tim         			= new Date().getTime();

	User.findOne({
		_id        : user_id,
		identifier : user_identifier,
		gid        : group_id,
		act        : true,
	},function(err,user){
		if(err){
			logger.error({"r":"init","msg":"user_find_failed","er":err,"p":req.body});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!user){
			logger.error({"r":"init","msg":"user_not_found","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		PluginPaidInfo.findOne({
			plugin_id : configs.PLUGIN_ID,
			gid       : group_id,
			act       : true
		},function(err,pluginIsPaid){
			if(err){
				logger.error({"r":"init","msg":"PluginInfo_find_failed","er":err,"p":req.body});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			var object_for_at = {
				id1 : user_id,
				id2 : server_id,
				id3 : configs.PLUGIN_ID,
				id4 : user.gid
			};
			var at;
			if(pluginIsPaid){
				console.log('inside pluginIsPaid');
				AuthSecurity.getSubscriptionAT(object_for_at, user_identifier, group_id, tim, function(err, at){
					if(err){
						logger.error({"r":"init","msg":"user_token_update_failed","er":err,"p":req.body});
						console.trace(err);
						if(err == "USER_SUBSCRIPTION_EXPIRED"){
							return sendError(res,"USER_SUBSCRIPTION_EXPIRED","user_subscription_ended",HttpStatuses.USER_SUBSCRIPTION_EXPIRED);
						}
						if(err == "USER_SUBSCRIPTION_REQUIRED"){
							return sendError(res,"USER_SUBSCRIPTION_EXPIRED","user_subscription_required",HttpStatuses.USER_SUBSCRIPTION_REQUIRED);
						}
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					logger.info({"r":"init","msg":"AuthSecurity_getSubscriptionAT_Success","p":req.body,at:at});
					return sendSuccess(res,{at : at});
				});
			} else {
				console.log('outside pluginIsPaid');
				at = AuthSecurity.getAT(object_for_at);
				updateUserToken(user_id, at, function(err, resp){
					if(err){
						logger.error({"r":"init","msg":"user_token_update_failed","er":err,"p":req.body});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					logger.info({"r":"init","msg":"AuthSecurity_getAT_Success","p":req.body,at:at});
					return sendSuccess(res,{at : at});
				});
			}
		});
	});
});

function updateUserToken(user_id, at, cb){
	UserToken.update({
		user_id : user_id,
		act     : true
	},{
		act : false
	},{
		multi : true
	},function(err,prev_updated){
		if(err){
			return cb(err,null);
		}
		var new_user_token = new UserToken({
			user_id     : user_id,
			act 		    : true,
			xcess_token : at
		});
		new_user_token.save(function(err, new_token){
			if(err){
				return cb(err,null);
			}
			return cb(null,true);
		});
	});
}

module.exports = router;